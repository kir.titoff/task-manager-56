package ru.t1.ktitov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.enumerated.Sort;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m";
        return entityManager.createQuery(jpql, TaskDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TaskDTO> criteriaQuery = criteriaBuilder.createQuery(TaskDTO.class);
        Root<TaskDTO> task = criteriaQuery.from(TaskDTO.class);
        criteriaQuery.select(task)
                .where(criteriaBuilder.equal(task.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(task.get(Sort.getOrderByField(sort))));
        TypedQuery<TaskDTO> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public TaskDTO findOneById(@Nullable final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.id = :id AND m.userIId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void clear() {
        @Nullable final List<TaskDTO> tasks = findAll();
        if (!tasks.isEmpty())
            tasks.forEach(entityManager::remove);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
