package ru.t1.ktitov.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;
import ru.t1.ktitov.tm.model.User;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Nullable
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null) return null;
        if (model == null) return null;
        model.setUser(entityManager.find(User.class, userId));
        return add(model);
    }

    @Nullable
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M remove(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || model == null) return null;
        entityManager.remove(model);
        return model;
    }

}
