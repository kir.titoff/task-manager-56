package ru.t1.ktitov.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.listener.EventListener;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.t1.ktitov.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private EventListener eventListener;

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUser());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getSecondLvlCache());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getProviderFilePath());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(SessionDTO.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        final EntityManagerFactory entityManagerFactory = metadata.getSessionFactoryBuilder().build();
        final SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        final EventListenerRegistry registryListener = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registryListener.getEventListenerGroup(EventType.POST_INSERT).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_DELETE).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_UPDATE).appendListener(eventListener);
        return entityManagerFactory;
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager() {
        return entityManagerFactory().createEntityManager();
    }

}
