package ru.t1.ktitov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.ktitov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    @Override
    @NotNull
    protected abstract IUserOwnedDtoRepository<M> getRepository();

    @Nullable
    @SneakyThrows
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            model.setUserId(userId);
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final M model = repository.findOneById(userId, id);
            if (model == null) throw new EntityNotFoundException();
            return model;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(userId, id) != null;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @SneakyThrows
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new EntityNotFoundException();
        if (!userId.equals(model.getUserId())) throw new EntityNotFoundException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final M model;
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            model = findOneById(userId, id);
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
