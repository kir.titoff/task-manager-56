package ru.t1.ktitov.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.api.repository.dto.IDtoRepository;
import ru.t1.ktitov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository
@Scope("prototype")
public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    public M update(@NotNull final M model) {
        return entityManager.merge(model);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }


    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        entityManager.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        remove(model);
        return model;
    }

    public void removeAll(@Nullable final List<M> modelsList) {
        if (modelsList == null) return;
        modelsList.forEach(this::remove);
    }

}
