package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataXmlJaxBSaveRequest;

@Component
public class DataXmlJaxBSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file by jaxb";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataXmlJaxBSaveRequest request = new DataXmlJaxBSaveRequest(getToken());
        domainEndpoint.saveDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
