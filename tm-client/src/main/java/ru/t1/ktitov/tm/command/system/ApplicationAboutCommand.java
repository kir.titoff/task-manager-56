package ru.t1.ktitov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.ktitov.tm.dto.response.system.ServerAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @Nullable final ServerAboutRequest request = new ServerAboutRequest();
        @NotNull final ServerAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
