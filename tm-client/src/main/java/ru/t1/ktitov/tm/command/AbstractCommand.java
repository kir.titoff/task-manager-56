package ru.t1.ktitov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.model.ICommand;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.api.service.ITokenService;
import ru.t1.ktitov.tm.enumerated.Role;

@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    public abstract void execute();

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result = result + name;
        if (argument != null && !argument.isEmpty()) result = result + ", " + argument;
        if (description != null && !description.isEmpty()) result = result + " - " + description;
        return result;
    }

}
