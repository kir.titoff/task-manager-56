package ru.t1.ktitov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataXmlJaxBLoadRequest;

@Component
public class DataXmlJaxBLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file by jaxb";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull DataXmlJaxBLoadRequest request = new DataXmlJaxBLoadRequest(getToken());
        domainEndpoint.loadDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
