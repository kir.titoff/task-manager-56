package ru.t1.ktitov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.command.AbstractCommand;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
